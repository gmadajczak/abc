import { Component } from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {Observable} from 'rxjs';
import { Message } from '../../interfaces/message';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
  content: string;
  messages$: Observable<Message[]>;
  constructor(private firebase: FirebaseService) {
    this.content = '';
    this.messages$ = firebase.messages$;
  }

  sendMessage() {
    this.firebase.sendMessage(this.content);
  }
}
