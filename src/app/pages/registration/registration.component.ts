import { Component } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { FirebaseService } from '../../services/firebase.service';
import { User } from '../../interfaces/user';
import { Router } from '@angular/router';
import {AlertServiceService} from '../../services/alert-service.service';
import { AlertSeverity } from '../../enums/alert-severity.enum';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  regForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private router: Router,
    private alertService: AlertServiceService
  ) {
    this.createForm();
  }

  /**
   * Creates user object based on form data
   * @returns {User}
   */
  get user(): User {
    const user: User = {
      name: this.regForm.get('name').value,
      lastname: this.regForm.get('lastname').value,
      email: this.regForm.get('email').value,
      password: this.regForm.get('password').value,
    };
    return user;
  }
  /**
   * Reactive form constructor
   */
  createForm(): void {
    this.regForm = this.fb.group({
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]],
    });
  }

  /**
   * Saves user to Firebase with own Firebase service
   */
  saveUser(): void {
    this.firebase.register(this.user)
      .then(() => {
        this.regForm.reset();
        this.router.navigate(['login']);
        this.alertService.push('OK', AlertSeverity.SUCCESS);
      })
      .catch((error: any) => {
        this.alertService.push(error.message, AlertSeverity.WARNING);
      });
  }

  /**
   * Hides error messages when invalid FormControl
   * @param {string} name
   * @returns {boolean}
   */
  hidden(name: string): boolean {
    const control: FormControl = <FormControl>this.regForm.get(name);
    return control.pristine || control.valid;
  }
}
