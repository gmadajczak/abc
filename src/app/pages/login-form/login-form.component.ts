import { Component } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';
import { AlertServiceService } from '../../services/alert-service.service';
import { AlertSeverity } from '../../enums/alert-severity.enum';

interface LoginData {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
  loginData: LoginData;
  constructor(
    private firebase: FirebaseService,
    private router: Router,
    private alertService: AlertServiceService
  ) {
    this.loginData = {
      email: '',
      password: '',
    };
  }

  /**
   * Log in
   */
  logIn() {
    this.firebase.login(this.loginData.email, this.loginData.password)
      .then((uid: string) => {
        this.firebase.userData(uid);
        this.router.navigate(['main']);
        this.alertService.push('Logowanie pomyślne', AlertSeverity.SUCCESS);
      })
      .catch((err: any) => {
        this.alertService.push(err.message, AlertSeverity.WARNING);
      });
  }
}
