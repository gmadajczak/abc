import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User;

  /**
   * Returns full name
   * @returns {string}
   */
  get fullName(): string {
    return this.user ? `${this.user.name} ${this.user.lastname}` : '';
  }

  /**
   * Ends session
   */
  logout(): void {
    this.user = undefined;
  }
}
