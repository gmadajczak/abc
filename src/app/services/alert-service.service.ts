import { Injectable } from '@angular/core';
import { AlertSeverity } from '../enums/alert-severity.enum';
import { Alert } from '../interfaces/alert';

@Injectable({
  providedIn: 'root'
})
export class AlertServiceService {
  private _alerts: Alert[];
  constructor() {
    this._alerts = [];
  }

  push(message: string, severity: AlertSeverity): boolean {
    const alert: Alert = {
      message,
      severity,
      id: new Date().toString()
    };
    const alertsLength: number = this._alerts.length;
    return this._alerts.push(alert) > alertsLength;
  }
  remove(id: string): void {
    const idx: number = this._alerts.findIndex((alert: Alert) => alert.id === id);
    if (idx !== -1) {
      this._alerts.splice(idx, 1);
    }
  }
  get alerts(): Alert[] {
    return this._alerts;
  }
}
