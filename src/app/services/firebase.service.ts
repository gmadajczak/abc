import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserService } from './user.service';
import { User } from '../interfaces/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  /**
   * Observable of messages collection
   */
  messages$: Observable<any[]>;
  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private afdb: AngularFireDatabase,
    private userService: UserService,
  ) {
    this.messages$ = afdb.list('messages').valueChanges();
  }

  /**
   * User registration with Angular FireStore and AngularAuth
   */
  register(user: User): Promise<any> {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then((resp: any) => {
        const userCollection: any = this.afs.collection('users');
        user.uid = resp.user.uid;
        delete user.password;
        return userCollection.add(user);
      });
  }

  /**
   * Login with Firebase auth
   * @param {string} email
   * @param {string} password
   * @returns {Promise<string>}
   */
  async login(email: string, password: string): Promise<string> {
    const uid: string = await this.afAuth.auth.signInWithEmailAndPassword(email, password).then((resp: any) => resp.user.uid);
    return uid;
  }

  /**
   * Set user data
   * @param {string} uid
   */
  userData(uid: string): void {
    this.afs.collection('users', ref => ref.where('uid', '==', uid))
      .valueChanges().subscribe((data: any) => { this.userService.user = data[0]; });
  }

  /**
   * Send new message to Firebase db
   * @param {string} content
   */
  sendMessage(content: string) {
    const refs = this.afdb.list('messages');
    refs.push({ content, user: this.userService.fullName });
  }
}
