export enum AlertSeverity {
  SUCCESS = 'alert-success',
  INFO = 'alert-info',
  WARNING = 'alert-warning'
}
