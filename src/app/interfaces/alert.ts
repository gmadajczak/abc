import {AlertSeverity} from '../enums/alert-severity.enum';

export interface Alert {
  message: string;
  severity: AlertSeverity;
  id: string;
}
