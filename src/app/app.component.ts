import { Component } from '@angular/core';
import { AlertServiceService } from './services/alert-service.service';
import { Alert } from './interfaces/alert';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private alertsService: AlertServiceService,
    private userService: UserService,
    private router: Router
    ) {}
  get fullname(): string { return this.userService.fullName; }
  get alerts(): Alert[] {
    return this.alertsService.alerts;
  }
  close(id: string): void {
    this.alertsService.remove(id);
  }
  logout() {
    this.userService.logout();
    this.router.navigate(['login']);
  }
}
